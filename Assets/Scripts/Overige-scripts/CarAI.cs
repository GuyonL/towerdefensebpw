﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarAI : MonoBehaviour {
    public GameObject[] WaypointCar;
    public int num = 0;

    public float minDist;
    public float speed;

    public bool rand = false;
    public bool go = true;
    // Use this fot initialization
    void Start()
    {

    }

    void Update()
    {
        float dist = Vector3.Distance(gameObject.transform.position, WaypointCar[num].transform.position);

        if (go)
        {
            if (dist > minDist)
            {
                Move();
            }
            else
            {
                if (!rand)
                {
                    if (num + 1 == WaypointCar.Length)
                    {
                        num = 0;
                    }
                    else
                    {
                        num++;
                    }
                }
                else
                {
                    num = Random.Range(0, WaypointCar.Length);
                }
            }
        }
    }

    public void Move()
    {
        gameObject.transform.LookAt(WaypointCar[num].transform.position);
        gameObject.transform.position += gameObject.transform.forward * speed * Time.deltaTime;
    }
}
